/** Graph.c
 * ===========================================================
 * Name: CS220, Spring 2019
 * Modified by: Manter & Drewes
 * Section: T3
 * Project: PEX4
 * Purpose: The implementation of a graph.
 * Documentation: None used
 * ===========================================================
 */

#include <stdlib.h>
#include <stdio.h>
#include <memory.h>
#include <math.h>
#include "graph.h"
#include "gameLogic.h"

/** -------------------------------------------------------------------
 * Create the memory needed to hold a graph data structure.
 * @param numberVertices the number of vertices in the graph
 * @param bytesPerNode the number of bytes used to represent a
 *                     single vertex of the graph
 * @return a pointer to a graph struct
 */
Graph *graphCreate(int numberVertices, int bytesPerNode) {
    // allocates memory for a graph pointer
    Graph *newGraph = malloc(sizeof(Graph));
    // assignes the number of vertexes in a graph to the given number
    newGraph->numberVertices = numberVertices;
    // allocats memory for the vertices base on number of vertexes and the size of each vertex
    newGraph->vertices = malloc(bytesPerNode * numberVertices);
    // allocates memory for the edge matrix
    newGraph->edges = malloc(numberVertices * bytesPerNode);
    for (int i = 0; i < numberVertices; i++) {
        newGraph->edges[i] = malloc(numberVertices * bytesPerNode);
    }
    return newGraph;
}

/** -------------------------------------------------------------------
 * Delete a graph data structure
 * @param graph the graph to delete
 */
void graphDelete(Graph *graph) {
    free(graph->vertices);
    for (int i = 0; i < graph->numberVertices; i++) {
        free(graph->edges[i]);
    }
    free(graph);
}

/** -------------------------------------------------------------------
 * Set the state of an edge in a graph
 * @param graph the graph to modify
 * @param fromVertex the beginning vertex of the edge
 * @param toVertex the ending vertex of the edge
 * @param state the state of the edge
 */
void graphSetEdge(Graph *graph, int fromVertex, int toVertex, int state) {
    graph->edges[fromVertex][toVertex] = state;
    graph->edges[toVertex][fromVertex] = state;
}

/** -------------------------------------------------------------------
 * Get the state of an edge in a graph
 * @param graph the graph
 * @param fromVertex the starting vertex of the edge
 * @param toVertex the ending vertex of the edge
 * @return the state of the edge
 */
int graphGetEdge(Graph *graph, int fromVertex, int toVertex) {
    int to = graph->edges[fromVertex][toVertex];
    int from = graph->edges[toVertex][fromVertex];
    if (to == from) {
        return to;
    }
    return -1;
}